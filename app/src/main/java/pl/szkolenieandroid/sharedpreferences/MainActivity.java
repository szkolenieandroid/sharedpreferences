package pl.szkolenieandroid.sharedpreferences;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

    public static final String TEXT = "text";
    @InjectView(R.id.text)
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        File file = new File(getFilesDir(), "edit_text_data.txt");

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            textView.setText(line);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        String value = textView.getText().toString();

        try {
            FileOutputStream outputStream = openFileOutput("edit_text_data.txt", Context.MODE_PRIVATE);
            outputStream.write(value.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.write_external_private)
    public void writeExternalPrivate() {
        File externalFilesDir = getExternalFilesDir(null);

        Log.e("###", "externalFilesDir: " + externalFilesDir.getAbsolutePath());
        File file = new File(externalFilesDir, "private_outptut.txt");
        try {
            Files.write(new Date().toString(), file, Charsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.write_external_public)
    public void writeExternalPublic() {
        File externalPublicFilesDir = Environment.getExternalStorageDirectory();

        Log.e("###", "getExternalStorageDirectory: " + externalPublicFilesDir.getAbsolutePath());
        File file = new File(externalPublicFilesDir, "public_outptut.txt");
        try {
            Files.write(new Date().toString(), file, Charsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }
}
